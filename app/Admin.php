<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use SoftDeletes;
    //
    public function author(){
        return $this->hasMany(Author::class,'admin_id','id');

    }
    // public function category(){
    //     return $this->belongsToMany(Category::class,AdminCategory::class,'admin_id','category_id');
    // }
    public function category()
    {
        return $this->belongsToMany(Category::class,AdminCategory::class,'admin_id','category_id');
    }
}
