<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Author extends Authenticatable
{
    //
    use SoftDeletes;
     public function admin(){
        return $this->belongsTo(Admin::class,'admin_id','id');

    }
}
