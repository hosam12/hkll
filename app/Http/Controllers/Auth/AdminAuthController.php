<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class AdminAuthController extends Controller
{
    //


        use AuthenticatesUsers;

      public function __construct()
      {
      $this->middleware('guest:admin')->except('logout');
    //   Auth::guest()
      }

      public function showLoginView(){

      return view('cms.auth.admin.login');
      }

      public function login(Request $request){

      $request->validate([
      'email'=>'required|email|string|exists:admins,email',
      'password'=>'required|string|min:6',
      'remember_me'=>'string|in:on'
      ]);



      $rememberMe = $request->remember_me == 'on' ? true : false;

      $credentials = [
      'email'=>$request->email,
      'password'=>$request->password,
      ];




      if (Auth::guard('admin')->attempt($credentials, $rememberMe)){
      $user = Auth::guard('admin')->user();
      if ($user->status == "Active"){


        // dd(App::getLocale());
      return redirect()->route('cms.dashboard');
      }
      else{
      Auth::guard('admin')->logout();
      return redirect()->guest(route('admin.blocked'));
      }

      }else{
      return redirect()->back()->withInput();
      }
      }

      public function logout(Request $request){
          if(Auth::guard('admin')->check()){
    Auth::guard('admin')->logout();
    $request->session()->invalidate();
    return redirect()->guest(route('admin.login_view'));

          }

      }

}
