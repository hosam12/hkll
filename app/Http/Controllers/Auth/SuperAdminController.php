<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class SuperAdminController extends Controller

{
    //

        use AuthenticatesUsers;

      public function __construct()
      {
      $this->middleware('guest:super')->except('logout');
      }
      public function d(){
          dd("fwekufhkjsnadj,f");
      }

      public function showLoginView(){

      return view('cms.auth.super.login');
      }

      public function login(Request $request){

      $request->validate([
      'email'=>'required|email|string|exists:super_admins,email',
      'password'=>'required|string',
      'remember_me'=>'string|in:on'
      ]);

        // dd(123);

      $rememberMe = $request->remember_me == 'on' ? true : false;

      $credentials = [
      'email'=>$request->email,
      'password'=>$request->password,
      ];




      if (Auth::guard('super')->attempt($credentials, $rememberMe)){
        //   dd(123);
      $user = Auth::guard('super')->user();
      return redirect()->route('cms.dashboard');

      }else{
      return redirect()->back()->withInput()->withErrors(['errors'=>'There Error from Password Or Email']);
      }
      }

      public function logout(Request $request){
          if(Auth::guard('super')->check()){
    Auth::guard('super')->logout();
    $request->session()->invalidate();
    return redirect()->guest(route('super.login_view'));

          }

      }

}
