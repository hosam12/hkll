<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthAdmin extends Controller
{

    public function __construct()
      {
      $this->middleware('guest:admin')->except('logout');
    //   Auth::guest()
      }

      public function showLoginView(){

      return view('cms.auth.admin.login');
      }


}
