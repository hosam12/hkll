<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class AuthorAuthController extends Controller
{
    //

        use AuthenticatesUsers;

      public function __construct()
      {
      $this->middleware('guest:author')->except('logout');
      }

      public function showLoginView(){
        //   dd(332);
      return view('cms.auth.author.login');
      }

      public function login(Request $request){

      $request->validate([
      'email'=>'required|email|string|exists:authors,email',
      'password'=>'required|string|min:6',
      'remember_me'=>'string|in:on'
      ]);



      $rememberMe = $request->remember_me == 'on' ? true : false;

      $credentials = [
      'email'=>$request->email,
      'password'=>$request->password,
      ];




      if (Auth::guard('author')->attempt($credentials, $rememberMe)){
      $user = Auth::guard('author')->user();
      if ($user->status == "Active"){


        // dd(App::getLocale());
      return redirect()->route('cms.dashboard');
      }
      else{
      Auth::guard('author')->logout();
      return redirect()->guest(route('author.blocked'));
      }

      }else{
      return redirect()->back()->withInput();
      }
      }

      public function logout(Request $request){
          if(Auth::guard('author')->check()){
    Auth::guard('author')->logout();
    $request->session()->invalidate();
    return redirect()->guest(route('author.login_view'));

          }

      }

}
