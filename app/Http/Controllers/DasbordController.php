<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Category;
use Illuminate\Http\Request;

class DasbordController extends Controller
{
    //
    function admin(){
        $count=Category::all()->count();
        $admins=Admin::all();

        return view('cms.dashbord',['count'=>$count,'admins'=>$admins]);
    }
}
