<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories=[];
        if(Auth::guard('super')->check()){
             $categories=Category::all();
        }
        else{
            $categories=Auth::user()->category;
        }

        return view('cms.categories.index',['categories'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // dd(88);
        //
        return view('cms.categories.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        //


        $cate= new Category();
        $cate->name=$request->get('name');
        $cate->status=$request->get('status')=='on'?'Visible':'InVisible';
          $token=Str::uuid();
        $cate->token=$token;

         if($request->hasFile('image')){
         $imagefile=$request->file('image');
         $imagename=time().'category'.$token.' '.' '.$imagefile->getClientOriginalName();

         $imagefile->move('images/categories',$imagename);
         $cate->image=$imagename ;
         }
        $save=  $cate->save();
      if($save){
          SuccessError::Success('تم الإنشاء بنجاح');
        return redirect()->back();
      }
      else{
          SuccessError::Error('فشل الإنشاء');
          return redirect()->back();

      }




    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($token)
    {
        //
        $category=Category::where('token',$token)->first();
        return view('cms.categories.edit',['category'=>$category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $token)
    {
        //
        $cate=Category::where('token',$token)->first();
        $cate->name=$request->get('name');
        $cate->status=$request->get('status')=='on'?'Visible':'InVisible';


         if($request->hasFile('image')){
         $imagefile=$request->file('image');
         $imagename=time().'category '.$token.' '.' '.$imagefile->getClientOriginalName();

         $imagefile->move('images/categories',$imagename);
         $cate->image=$imagename ;
         }
        $save=  $cate->save();
      if($save){
          SuccessError::Success('تم التعديل بنجاح');
        return redirect()->back();
      }
      else{
          SuccessError::Error('فشل التعديل');
          return redirect()->back();

      }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($token)
    {

        $category=Category::where('token',$token)->first();

        if(!$category){
             return response()->json(['icon'=>'warning','title'=>'الفئة غير موجودة'],400);
        }
        else{
         $del=   $category->delete($category->id);
   if ($del){
              return response()->json(['icon'=>'success','title'=>'تم الحذف بنجاح  '],200);
              }else{
              return response()->json(['icon'=>'error','title'=>'فشل الحذف'],400);
              }
        }




    }
    public function gr(){
        return 5;
    }
}
