<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Author;
use App\Http\Requests\AuthorRequest;
use Faker\Provider\Uuid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Ramsey\Uuid\Type\Integer;

class AuthorController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $authors=[];
        if(Auth::guard('super')->check()){
         $authors=Author::all();
        }
        else{
            $authors=Auth::user()->author;
        }

        return view('cms.authors.index',['authors'=>$authors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(Auth::guard('super')){


        return view('cms.authors.create',['admins'=>Admin::where('status','Active')->get(['name','id'])]);
    }
    else{
        return view('cms.authors.create');
    }

}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AuthorRequest $request)
    {
        if(Auth::guard('super')->check()){
            $request->validate([
            'admin_id'=>'required'

            ],[
             'admin_id.required'=>'يجب تحديد أدمن للكاتب'
            ]);
        }
         $author= new Author();
        $author->name=$request->get('name');
        $author->email=$request->get('email');
        $author->mobile=$request->get('mobile');
        $author->address=$request->get('address');
        $author->status=$request->get('status')=='on'?'Active':'Blocked';
        $author->password=Hash::make($request->get('password'));
        $token=Str::uuid();
        $author->token=$token;
        if(Auth::guard('super')->check()){

        $author->admin_id=$request->get('admin_id');
        }
        else{
            $author->admin_id=Auth::id();
        }

         if($request->hasFile('image')){
         $imagefile=$request->file('image');
         $imagename=time().$token.$imagefile->getClientOriginalName();

         $imagefile->move('images/authors',$imagename);
         $author->image=$imagename ;
         }
         $save=$author->save();
         if($save){
            SuccessError::Success('تم الإنشاء بنجاح');
            return redirect()->back();
      }
      else{
          SuccessError::Error('فشل الإنشاء');
          return redirect()->back();

      }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit($token)
    {

        $author=Author::where('token',$token)->first();
        return view('cms.authors.edit',['author'=>$author]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $token)
    {

              $author=Author::where('token',$token)->first();
        $id=$author->id;

        $request->validate([
            'email' => 'required:string|email|unique:admins,email|unique:authors,email,' . $id,
            'mobile' => 'required|numeric|unique:admins,mobile|unique:authors,mobile,'.$id,

        ],[
            'mobile.required' => 'الهاتف مطلوب',
            'mobile.numeric' => ' يجب أن يكون أرقام',
            'mobile.unique' => 'تم استخدام هذا الرقم من قبل',
             'email.required' => 'الإيميل مطلوب',
            'email.email' => 'الإيميل خاطئ',
            'email.unique' => 'هذا الحساب مسجل من قبل ',
        ]);

        $author->name=$request->get('name');
        $author->email=$request->get('email');
        $author->mobile=$request->get('mobile');
        $author->address=$request->get('address');
        $author->status=$request->get('status')=='on'?'Active':'Blocked';
         if($request->hasFile('image')){
         $imagefile=$request->file('image');
         $imagename=time().' '.'authors_image'.$author->token.' '.$imagefile->getClientOriginalName();

         $imagefile->move('images/authors',$imagename);
         $author->image=$imagename ;
         }
         $save=$author->save();
         if($save){
            SuccessError::Success('تم التعديل بنجاح');
            return redirect()->back();
      }
      else{
          SuccessError::Error('فشل التعديل');
          return redirect()->back();

      }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy($token)
    {
        //
         $author=Author::where('token',$token)->delete();
        //  $del=   $admin->delete();
   if ($author){
              return response()->json(['icon'=>'success','title'=>'تم الحذف بنجاح  '],200);
              }else{
              return response()->json(['icon'=>'error','title'=>'فشل الحذف'],400);
              }

    }


     public function AuthorPassword(Request $request,$token){


        $request->validate([
             'password' => 'min:8',
            'confirm'=>'same:password',

        ],[
            'password.min' => 'كلمة السر يجب أن تتجاوز 8 حروف',
            'confirm.same'=>'يجب أن تكون الكلمتان متطابقتان'
        ]);

            // dd($request->token);
        $user = Author::where('token',$token)->first();
        if($user){

             $user->password = Hash::make($request->get('password'));
        $isSaved = $user->save();
        if ($isSaved) {
            return response()->json(['icon' => 'success', 'title' => 'تم تغيير كلمة السر بنجاح'], 200);
        } else {
            return response()->json(['icon' => 'success', 'title' => 'فشل تغيير كلمة السر'], 400);
        }

        }
        else{
                        return response()->json(['icon' => 'warning', 'title' => 'المستخدم غير موجود'], 400);

        }


    }
}
