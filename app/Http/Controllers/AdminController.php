<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Category;
use App\Http\Requests\AdminEditRequest;
use App\Http\Requests\AdminRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $admins=Admin::withCount(['category','author'])->get();
        return view('cms.admins.index',['admins'=>$admins]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cms.admins.create',['categorys'=>Category::all()]);
    }

    public function AdminCategory($token){

  $categories=Admin::where('token',$token)->first()->category()->get();

                return view('cms.categories.index',['categories'=>$categories]);

}
  public function AdminAuthor($token){

  $categories=Admin::where('token',$token)->first()->author()->get();

                return view('cms.authors.index',['authors'=>$categories]);

}


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {
       $admin= new Admin();
        $admin->name=$request->get('name');
        $admin->email=$request->get('email');
        $admin->mobile=$request->get('mobile');
        $admin->address=$request->get('address');
        $admin->status=$request->get('status')=='on'?'Active':'Blocked';
        $admin->password=Hash::make($request->get('password'));
        $token=Str::uuid();
        $admin->token=$token;
         if($request->hasFile('image')){
         $imagefile=$request->file('image');
         $imagename=time().' '.$token.' '.' '.$imagefile->getClientOriginalName();

         $imagefile->move('images/admins',$imagename);
         $admin->image=$imagename ;
         }
         $save=$admin->save();
         if($save){
        $admin->category()->sync($request->get('category_id'));

            SuccessError::Success('تم الإنشاء بنجاح');
            return redirect()->back();
      }
      else{
          SuccessError::Error('فشل الإنشاء');
          return redirect()->back();

      }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(Admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit($token)
    {
        $admin=Admin::where('token',$token)->with('category')->first();
        return view('cms.admins.edit',['admin'=>$admin,'categorys'=>Category::all()]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(AdminEditRequest $request, $token)
    {
        //
        // dd(444);

        $admin=Admin::where('token',$token)->first();
        $id=$admin->id;

        $request->validate([
            'email' => 'required:string|email|unique:authors,email|unique:admins,email,' . $id,
            'mobile' => 'required|numeric|unique:authors,mobile|unique:admins,mobile,'.$id,

        ],[
            'mobile.required' => 'الهاتف مطلوب',
            'mobile.numeric' => ' يجب أن يكون أرقام',
            'mobile.unique' => 'تم استخدام هذا الرقم من قبل',
             'email.required' => 'الإيميل مطلوب',
            'email.email' => 'الإيميل خاطئ',
            'email.unique' => 'هذا الحساب مسجل من قبل ',
        ]);

        $admin->name=$request->get('name');
        $admin->email=$request->get('email');
        $admin->mobile=$request->get('mobile');
        $admin->address=$request->get('address');
        $admin->status=$request->get('status')=='on'?'Active':'Blocked';
         if($request->hasFile('image')){
         $imagefile=$request->file('image');
         $imagename=time().' '.'admins_image'.$admin->token.' '.$imagefile->getClientOriginalName();

         $imagefile->move('images/admins',$imagename);
         $admin->image=$imagename ;
         }
         $save=$admin->save();
         if($save){
            $admin->category()->sync($request->get('category_id'));
            SuccessError::Success('تم التعديل بنجاح');
            return redirect()->back();
      }
      else{
          SuccessError::Error('فشل التعديل');
          return redirect()->back();

      }







    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy($token)
    {
//  $admin=Admin::where('token',$token)->first();


       $admin=Admin::where('token',$token)->delete();
        //  $del=   $admin->delete();
   if ($admin){
              return response()->json(['icon'=>'success','title'=>'تم الحذف بنجاح  '],200);
              }else{
              return response()->json(['icon'=>'error','title'=>'فشل الحذف'],400);
              }

    }

    public function AdminPassword(Request $request,$token){

        $request->validate([
             'password' => 'min:8',
            'confirm'=>'same:password',

        ],[
            'password.min' => 'كلمة السر يجب أن تتجاوز 8 حروف',
            'confirm.same'=>'يجب أن تكون الكلمتان متطابقتان'
        ]);

            // dd($request->token);
        $user = Admin::where('token',$token)->first();
        if($user){

             $user->password = Hash::make($request->get('password'));
        $isSaved = $user->save();
        if ($isSaved) {
            return response()->json(['icon' => 'success', 'title' => 'تم تغيير كلمة السر بنجاح'], 200);
        } else {
            return response()->json(['icon' => 'success', 'title' => 'فشل تغيير كلمة السر'], 400);
        }

        }
        else{
                        return response()->json(['icon' => 'warning', 'title' => 'المستخدم غير موجود'], 400);

        }


    }
}
