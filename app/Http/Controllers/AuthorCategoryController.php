<?php

namespace App\Http\Controllers;

use App\AuthorCategory;
use Illuminate\Http\Request;

class AuthorCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AuthorCategory  $authorCategory
     * @return \Illuminate\Http\Response
     */
    public function show(AuthorCategory $authorCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AuthorCategory  $authorCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(AuthorCategory $authorCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AuthorCategory  $authorCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AuthorCategory $authorCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AuthorCategory  $authorCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(AuthorCategory $authorCategory)
    {
        //
    }
}
