<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    protected function redirectTo($request)
    {
        // if (! $request->expectsJson()) {
        //     return route('login');
        // }
        // return redirect()->back();

         if ($this->auth->guard('author')) {


            $loginRoute = "author.login_view";
            return route($loginRoute);

        } else if($this->auth->guard('admin')){
            $loginRoute = "admin.login_view";
            return route($loginRoute);
        }
        else if($this->auth->guard('super')){
            $loginRoute = "super.login_view";
            return route($loginRoute);
        }
    }
}
