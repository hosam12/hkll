<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:8|max:45',
            'email' => 'required|email|unique:admins,email|unique:authors,email',
            'mobile' => 'required|numeric|unique:admins,mobile|unique:authors,mobile',
            'image' => 'required|image',
            'address' => 'required|string|min:10|max:45',
            'password' => 'required|string|min:8',
            'password2'=>'same:password',
            'status' => 'string|in:on',


        ];
    }
      public function messages()
    {
        return [

            'name.required' => 'الاسم مطلوب',
            'mobile.required' => 'الهاتف مطلوب',
            'mobile.numeric' => ' يجب أن يكون أرقام',
            'mobile.unique' => 'تم استخدام هذا الرقم من قبل',
            'name.min' => 'أحرف الاسم أقل من 8 ',
            'name.max' => 'أحرف الاسم أكبر من 45 ',
            'image.required' => 'الرجاء رفع صورة',
            'image.image' => 'يجب أن تكون صورة',
            'numberid.unique' => 'هذا الحساب مسجل من قبل ',
            'email.required' => 'الإيميل مطلوب',
            'email.email' => 'الإيميل خاطئ',
            'email.unique' => 'هذا الحساب مسجل من قبل ',
            'address.required' => 'العنوان مطلوب',
            'address.min' => 'أحرف العنوان أقل من 10',
            'address.max' => 'أحرف العنوان أكبر من 45 ',
            'password.required' => 'كلمة السر مطلوبة',
            'password.min' => 'كلمة السر يجب أن تتجاوز 8 حروف',
            'password2.same'=>'يجب أن تكون الكلمتان متطابقتان',
        ];
    }
}
