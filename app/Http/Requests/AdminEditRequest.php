<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use phpDocumentor\Reflection\PseudoTypes\True_;

class AdminEditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:8|max:45',
            'image' => 'image',
            'address' => 'required|string|min:10|max:45',
            'status' => 'string|in:on',


        ];


    }
     public function messages()
    {
        return [

            'name.required' => 'الاسم مطلوب',
            'name.min' => 'أحرف الاسم أقل من 8 ',
            'name.max' => 'أحرف الاسم أكبر من 45 ',
            'image.image' => 'يجب أن تكون صورة',
            'address.required' => 'العنوان مطلوب',
            'address.min' => 'أحرف العنوان أقل من 10',
            'address.max' => 'أحرف العنوان أكبر من 45 ',

        ];
    }

}
