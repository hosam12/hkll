<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    //
    use SoftDeletes;
 public function admins(){
        return $this->belongsToMany(Admin::class,AdminCategory::class,'category_id','admin_id');
    }

}
