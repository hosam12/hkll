<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors', function (Blueprint $table) {
            // $table->uuid('sgd');
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('mobile')->unique();
            $table->uuid('token');
            $table->enum('status', ['Active', 'Blocked'])->default('Active');
            $table->string('address')->nullable();
            $table->string('image', 255)->nullable();
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->softDeletes();
            $table->unsignedBigInteger('admin_id')->nullable();
            $table->string('now')->nullable();
            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authors');
    }
}
