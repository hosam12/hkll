<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return get('','');
// });

//****************** CMS-ADMIN *******************//

    Route::get('','DasbordController@admin')->name('cms.dashboard')->middleware('auth:author,super,admin');


Route::prefix('cms/')->group(function () {


    Route::resource('author', 'AuthorController')->middleware('auth:super,admin');

    Route::resource('category', 'CategoryController')->middleware('auth:admin,super');
    Route::resource('super', 'SuperAdminController')->middleware('auth:super');
    Route::resource('admin', 'AdminController')->middleware('auth:super');
    Route::put('admin/Password/{token}', 'AdminController@AdminPassword')->name('admin.password')->middleware('auth:admin,super');
    Route::put('author/Password/{token}', 'AuthorController@AuthorPassword')->name('author.password')->middleware('auth:admin,super');
    Route::get('/admin/CategoryAdmins/{token}','AdminController@AdminCategory')->name('admin.category')->middleware('auth:super');
    Route::get('/admin/AuthorAdmins/{token}','AdminController@AdminAuthor')->name('admin.author')->middleware('auth:super');
    Route::get('logouts', 'Auth\SuperAdminController@logout')->name('super.logout')->middleware('auth:super');
    Route::get('logouta', 'Auth\AdminAuthController@logout')->name('admin.logout')->middleware('auth:admin');
    Route::get('logoutA', 'Auth\AuthorAuthController@logout')->name('author.logout')->middleware('auth:author');



});


Route::prefix('super/')->namespace('Auth')->group(function () {
Route::get('login', 'SuperAdminController@showLoginView')->name('super.login_view');
Route::post('login', 'SuperAdminController@login')->name('super.post');


});

Route::prefix('admin/')->namespace('Auth')->group(function () {
Route::get('login', 'AdminAuthController@showLoginView')->name('admin.login_view');
Route::post('login', 'AdminAuthController@login')->name('admin.post');

});
// Route::get('login', 'AuthorAuthController@showLoginView')->name('author.login_view');

Route::prefix('author')->namespace('Auth')->group(function () {
Route::get('login', 'AuthorAuthController@showLoginView')->name('author.login_view');
Route::post('login', 'AuthorAuthController@login')->name('author.post');

});



Route::get('/h', function () {
    return view('cms.auth.super.login');
});



// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
